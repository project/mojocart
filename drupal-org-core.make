api = 2
core = 7.x

; Drupal Core
projects[drupal][type] = core
projects[drupal][version] = 7.41
projects[drupal][patch][728702] = "http://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"
